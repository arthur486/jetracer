/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.tool.jetracer.demo.gui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import edu.art.module.jetracer.agent.InstrumentConfig;
import edu.art.module.jetracer.common.EventMessage;
import edu.art.module.jetracer.host.EventMessagesWrapper;

public class Util {

	public static void startGUI(final InstrumentConfig config) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					new MainWindow(config);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void startGUI(final File inputFile) {
		//
		// Read and filter input file
		//
		final List<EventMessage> events = Util.read(inputFile);
		events.remove(events.size() - 1);
		filterDoneMessages(events);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					new MainWindow(events);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void filterDoneMessages(List<EventMessage> list) {
		Iterator<EventMessage> it = list.iterator();

		while (it.hasNext()) {
			if (it.next().isDone == true) {
				it.remove();
			}
		}
	}

	public static List<EventMessage> read(File traceFile) {
		//
		// 1. Unmarshal the events file
		//
		EventMessagesWrapper event = (EventMessagesWrapper) unmarshall(traceFile, EventMessagesWrapper.class);
		if (event == null) {
			return null;
		}

		//
		// 2. Add the screenshots
		//
		File scrDirectory = traceFile.getParentFile();
		for (EventMessage message : event.events) {
			if (message.isDone == false) {
				message.screenShot = loadScreenShot(scrDirectory, "scr_" + message.id + ".png");
			}
		}
		return event.events;
	}

	private static BufferedImage loadScreenShot(File dir, String fileName) {
		File scr = new File(dir.getAbsoluteFile() + "/" + fileName);
		if (scr.exists() == false) {
			return null;
		}
		try {
			return ImageIO.read(scr);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	private static synchronized Object unmarshall(File file, Class clazz) {
		try {
			Unmarshaller unmarshaller = JAXBContext.newInstance(clazz).createUnmarshaller();
			return unmarshaller.unmarshal(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
