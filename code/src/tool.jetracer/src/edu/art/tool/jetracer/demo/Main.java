/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.tool.jetracer.demo;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import edu.art.module.jetracer.EventGranularity;
import edu.art.module.jetracer.agent.InstrumentConfig;
import edu.art.module.jetracer.host.WidgetInstrumentationConnector;
import edu.art.module.jetracer.host.WidgetInstrumentationService;
import edu.art.tool.jetracer.demo.gui.Util;

public class Main {
	//
	// Option to start the GUI
	//
	@Option(name = "-gui", usage = "Option to start GUI. GUI will be started regardless of this option's value if -input is given")
	private boolean startGUI = false;

	//
	// Input file for offline analysis
	//
	@Option(name = "-input", usage = "Input file of event trace")
	private String traceInputFile = null;

	//
	// Following options are for live analysis
	//
	private InstrumentConfig config = new InstrumentConfig();

	@Option(name = "-scr", usage = "Set true if screenshots will be transmitted")
	private void setIncludeScreenshots(boolean includeScreenshots) {
		config.setIncludeScreenshots(includeScreenshots);
	}

	@Option(name = "-port", usage = "Network port")
	private int port = 6000;

	@Option(name = "-ap", usage = "Semicolon separated list of application packages")
	public void setApplicationPackages(String applicationPackages) {
		if (applicationPackages != null && applicationPackages.length() > 0) {
			config.setApplicationPackages(new HashSet<String>(Arrays.asList(applicationPackages.split(";"))));
		}
	}

	@Option(name = "-eg", usage = "Granularity of recorded events. One of all, any or actioned")
	public void setEventGranularity(String eventGranularity) throws CmdLineException {
		if (eventGranularity == null) {
			return;
		}
		if ("all".equals(eventGranularity.toLowerCase())) {
			config.setEventGranularity(EventGranularity.All);
			return;
		}
		if ("any".equals(eventGranularity.toLowerCase())) {
			config.setEventGranularity(EventGranularity.AnyListener);
			return;
		}
		if ("actioned".equals(eventGranularity.toLowerCase())) {
			config.setEventGranularity(EventGranularity.ActionedListener);
			return;
		}
		throw new IllegalArgumentException("Bad command line argument for -eg");
	}

	@Option(name = "-filter", usage = "Comma separated list of events that will NOT be recorded")
	private void setEventFilter(String eventFilter) {
		if (eventFilter != null && eventFilter.length() > 0) {
			config.setFilteredEvents(new HashSet<String>(Arrays.asList(eventFilter.split(";"))));
		}
	}

	@Option(name = "-output", usage = "File where event trace is recorded")
	private void setOutput(String filePath) {
		File output = new File(filePath);
		config.setOutput(output);
	}

	public void start(String[] args) throws CmdLineException {
		//
		// Parse command line
		//
		CmdLineParser parser = new CmdLineParser(this);
		parser.setUsageWidth(80);
		parser.parseArgument(args);

		//
		// Figure out what to start
		//

		// If no GUI then we are recording a live trace to -output file
		if (startGUI == false) {
			WidgetInstrumentationService wis = new WidgetInstrumentationService();
			WidgetInstrumentationConnector wisConnector = (WidgetInstrumentationConnector) wis.configure(config);
			wisConnector.connect();
		}

		File traceInput = null;
		// If -input is provided then will start GUI
		if (traceInputFile != null && traceInputFile.length() > 0) {
			traceInput = new File(traceInputFile);
			startGUI = true;
		}

		// Decide what to do if -gui given
		if (startGUI == true) {
			// If event trace is provided start GUI in offline mode
			if (traceInput != null) {
				Util.startGUI(traceInput);
			} else
			// Start GUI for live tracing
			{
				Util.startGUI(config);
			}
		}
	}

	public static void main(String[] args) throws CmdLineException {
		new Main().start(args);
	}
}
