/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.tool.jetracer.demo.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.Scrollable;

import edu.art.module.jetracer.common.EventMessage;

@SuppressWarnings("serial")
public class EventMessagePanel extends JPanel {

	private JTabbedPane tabbedPane;

	private EventMessage msg;

	private ScrollLabel label = null;

	public EventMessagePanel(EventMessage msg) {
		this.msg = msg;
		initGUI();
	}

	private void initGUI() {
		tabbedPane = new JTabbedPane();
		tabbedPane.add(msg.id + " event properties", internalCreatePropertiesPanel());
		if (msg.screenShot != null) {
			System.out.println("adding screenshot");
			tabbedPane.add("Widget screenshot", internalCreateScreenshotPanel());
		}
		setLayout(new BorderLayout());
		add(tabbedPane);
		validate();
	}

	private JPanel internalCreateScreenshotPanel() {
		JPanel scrPanel = new JPanel();
		scrPanel.setLayout(new BorderLayout());
		scrPanel.setBorder(BorderFactory.createLineBorder(Color.red));

		label = new ScrollLabel(msg.screenShot);
		Rectangle rect = markComponentOnScreenshot(msg);
		JScrollPane scrollPane = new JScrollPane(label);
		scrPanel.add(scrollPane, BorderLayout.CENTER);

		//
		// Scroll viewport to center widget
		//
		Dimension viewSize = label.getPreferredScrollableViewportSize();
		rect.x -= viewSize.width / 2 - rect.width / 2;
		rect.y -= viewSize.height / 2 - rect.height / 2;
		label.scrollRectToVisible(rect);
		return scrPanel;
	}

	private JPanel internalCreatePropertiesPanel() {
		JPanel p = new JPanel();
		p.setLayout(new BorderLayout());

		Object[][] rowData = getRowData();
		String[] columnNames = new String[] { "Property", "Value" };

		JTable propTable = new JTable(rowData, columnNames);
		p.add(propTable);
		return p;
	}

	private Object[][] getRowData() {
		// Allocate enough space for table data
		int lCount = 0;
		for (String[] lNames : msg.listeners.values()) {
			lCount += lNames.length;
		}
		Object[][] result = new Object[7 + lCount][2];

		//
		// Add simple properties
		//
		result[0][0] = "Id";
		result[0][1] = msg.id;
		result[1][0] = "Class";
		result[1][1] = msg.clazz;
		result[2][0] = "Widget index";
		result[2][1] = msg.index;
		result[3][0] = "Widget location";
		result[3][1] = "(" + msg.x + "," + msg.y + ")";
		result[4][0] = "Widget size";
		result[4][1] = "(" + msg.width + "," + msg.height + ")";
		result[5][0] = "Fired at";
		result[5][1] = new Date(msg.eventFiredTime);
		result[6][0] = "Type";
		result[6][1] = msg.type;

		//
		// Add listeners
		//
		int index = 7;
		for (String lType : msg.listeners.keySet()) {
			String[] lClasses = msg.listeners.get(lType);
			for (int i = index; i < lClasses.length + index; i++) {
				result[i][0] = lType;
				result[i][1] = lClasses[i - index];
			}
			index += lClasses.length;
		}
		return result;
	}

	private static Rectangle markComponentOnScreenshot(EventMessage e) {
		Rectangle rect = new Rectangle(e.x, e.y, e.width, e.height);
		if (e.screenShot == null) {
			return rect;
		}
		Graphics2D g = (Graphics2D) e.screenShot.getGraphics();
		g.setColor(Color.BLACK);
		g.drawRect(e.x, e.y, e.width, e.height);
		g.drawRect(e.x - 1, e.y - 1, e.width + 2, e.height + 2);
		g.drawRect(e.x - 2, e.y - 2, e.width + 3, e.height + 3);
		g.drawRect(e.x - 4, e.y - 4, e.width + 7, e.height + 7);
		return rect;
	}
}

@SuppressWarnings("serial")
class ScrollLabel extends JLabel implements Scrollable {

	public ScrollLabel(Image icon) {
		super(new ImageIcon(icon));
	}

	@Override
	public Dimension getPreferredScrollableViewportSize() {
		return new Dimension(200, 200);
	}

	@Override
	public Dimension getMaximumSize() {
		return new Dimension(getIcon().getIconWidth(), getIcon().getIconHeight());
	}

	@Override
	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 10;
	}

	@Override
	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
		return visibleRect.width;
	}

	@Override
	public boolean getScrollableTracksViewportWidth() {
		return false;
	}

	@Override
	public boolean getScrollableTracksViewportHeight() {
		return false;
	}
}