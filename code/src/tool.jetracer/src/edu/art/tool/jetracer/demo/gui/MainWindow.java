/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.tool.jetracer.demo.gui;

import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import edu.art.module.jetracer.agent.InstrumentConfig;
import edu.art.module.jetracer.common.EventMessage;
import edu.art.module.jetracer.common.ExitEventMessage;
import edu.art.module.jetracer.host.WidgetInstrumentationConnector;
import edu.art.module.jetracer.host.WidgetInstrumentationService;
import edu.art.module.jetracer.host.server.event.EventMessageListener;
import edu.art.module.jetracer.host.server.event.EventMessageReceivedEvent;

@SuppressWarnings("serial")
public class MainWindow extends JFrame implements EventMessageListener {

	private List<EventMessage> events = new LinkedList<EventMessage>();

	private JPanel mainPanel;

	public MainWindow(InstrumentConfig config) {
		super("JETracer - live");

		WidgetInstrumentationService wis = new WidgetInstrumentationService();
		WidgetInstrumentationConnector wisConnector = (WidgetInstrumentationConnector) wis.configure(config);
		wisConnector.addEventMessageListener(this);
		wisConnector.connect();
		initGUI();
	}

	public MainWindow(List<EventMessage> events) {
		super("JETracer - offline");

		this.events = events;
		initGUI();
	}

	private void initGUI() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(600, 480);

		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		JScrollPane mainSP = new JScrollPane(mainPanel);
		add(mainSP);

		for (EventMessage em : events) {
			EventMessagePanel panel = new EventMessagePanel(em);
			mainPanel.add(panel);
		}

		pack();
		setVisible(true);
	}

	@Override
	public void messageReceived(EventMessageReceivedEvent event) {
		EventMessage msg = event.getMessage();

		if (msg instanceof ExitEventMessage) {
			JOptionPane.showMessageDialog(this, "Target application disconnected", "Disconnect", JOptionPane.WARNING_MESSAGE);
			return;
		}

		if (event.getMessage().isDone == false) {
			mainPanel.add(new EventMessagePanel(event.getMessage()));
		}
	}
}
