/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.agent;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TypedListener;
import org.eclipse.swt.widgets.Widget;

import edu.art.module.jetracer.agent.DefaultSocketClient;
import edu.art.module.jetracer.common.EventMessage;
import edu.art.module.jetracer.handlers.HandlerInfoService;

public class SWTSocketClient extends DefaultSocketClient {

	private static int messageId = 1;

	private static Display display;

	//
	// This is a dummy listener to force creation of SWT's EventTable for widgets.
	//
	private static Listener filterListener = new Listener() {
		@Override
		public void handleEvent(Event event) {
		}
	};

	public static Map<Integer, String> eventNameMap;

	static {
		//
		// 1. Initialize
		//
		initEventNameMap();
		display = Display.getCurrent();

		//
		// 2. Set up dummy filter
		//
		for (Integer event : eventNameMap.keySet()) {
			display.addFilter(event, filterListener);
		}
	}

	public static String getEventHandler(String event) {
		if (event == null) {
			return null;
		}
		return HandlerInfoService.getHandlerInfo().getBundle("SWT").findEventHandler(event).getName();
	}

	private static void initEventNameMap() {
		if (eventNameMap == null) {
			eventNameMap = new HashMap<Integer, String>();
			// eventNameMap.put(0, "org.eclipse.swt.events.None");
			eventNameMap.put(1, "org.eclipse.swt.events.KeyDown");
			eventNameMap.put(2, "org.eclipse.swt.events.KeyUp");
			eventNameMap.put(3, "org.eclipse.swt.events.MouseDown");
			eventNameMap.put(4, "org.eclipse.swt.events.MouseUp");
			eventNameMap.put(5, "org.eclipse.swt.events.MouseMove");
			eventNameMap.put(6, "org.eclipse.swt.events.MouseEnter");
			eventNameMap.put(7, "org.eclipse.swt.events.MouseExit");
			eventNameMap.put(8, "org.eclipse.swt.events.MouseDoubleClick");
			eventNameMap.put(9, "org.eclipse.swt.events.Paint");
			eventNameMap.put(10, "org.eclipse.swt.events.Move");
			eventNameMap.put(11, "org.eclipse.swt.events.Resize");
			eventNameMap.put(12, "org.eclipse.swt.events.Dispose");
			eventNameMap.put(13, "org.eclipse.swt.events.Selection");
			eventNameMap.put(14, "org.eclipse.swt.events.DefaultSelection");
			eventNameMap.put(15, "org.eclipse.swt.events.FocusIn");
			eventNameMap.put(16, "org.eclipse.swt.events.FocusOut");
			eventNameMap.put(17, "org.eclipse.swt.events.Expand");
			eventNameMap.put(18, "org.eclipse.swt.events.Collapse");
			eventNameMap.put(19, "org.eclipse.swt.events.Iconify");
			eventNameMap.put(20, "org.eclipse.swt.events.Deiconify");
			eventNameMap.put(21, "org.eclipse.swt.events.Close");
			eventNameMap.put(22, "org.eclipse.swt.events.Show");
			eventNameMap.put(23, "org.eclipse.swt.events.Hide");
			eventNameMap.put(24, "org.eclipse.swt.events.Modify");
			eventNameMap.put(25, "org.eclipse.swt.events.Verify");
			eventNameMap.put(26, "org.eclipse.swt.events.Activate");
			eventNameMap.put(27, "org.eclipse.swt.events.Deactivate");
			eventNameMap.put(28, "org.eclipse.swt.events.Help");
			eventNameMap.put(29, "org.eclipse.swt.events.DragDetect");
			eventNameMap.put(30, "org.eclipse.swt.events.Arm");
			eventNameMap.put(31, "org.eclipse.swt.events.Traverse");
			eventNameMap.put(32, "org.eclipse.swt.events.MouseHover");
			eventNameMap.put(33, "org.eclipse.swt.events.HardKeyDown");
			eventNameMap.put(34, "org.eclipse.swt.events.HardKeyUp");
			eventNameMap.put(35, "org.eclipse.swt.events.MenuDetect");
			eventNameMap.put(36, "org.eclipse.swt.events.SetData");
			eventNameMap.put(37, "org.eclipse.swt.events.MouseVerticalWheel");
			eventNameMap.put(38, "org.eclipse.swt.events.MouseHorizontalWheel");
			eventNameMap.put(39, "org.eclipse.swt.events.Settings");
			eventNameMap.put(40, "org.eclipse.swt.events.EraseItem");
			eventNameMap.put(41, "org.eclipse.swt.events.MeasureItem");
			eventNameMap.put(42, "org.eclipse.swt.events.PaintItem");
			eventNameMap.put(43, "org.eclipse.swt.events.ImeComposition");
			eventNameMap.put(44, "org.eclipse.swt.events.OrientationChange");
			eventNameMap.put(45, "org.eclipse.swt.events.Skin");
			eventNameMap.put(46, "org.eclipse.swt.events.OpenDocument");
			eventNameMap.put(47, "org.eclipse.swt.events.Touch");
			eventNameMap.put(48, "org.eclipse.swt.events.Gesture");
			eventNameMap.put(49, "org.eclipse.swt.events.Segments");
			eventNameMap.put(50, "org.eclipse.swt.events.PreEvent");
			eventNameMap.put(51, "org.eclipse.swt.events.PostEvent");
			eventNameMap.put(52, "org.eclipse.swt.events.Sleep");
			eventNameMap.put(53, "org.eclipse.swt.events.Wakeup");
		}
	}

	protected static boolean applyFilter(EventMessage em) {
		return applyListenerFilter("SWT", em) || applyEventFilter(em);
	}

	protected static EventMessage prepareMessage(Display d, Event event) throws Exception {
		//
		// 1. Create message object
		//
		EventMessage e = new EventMessage();
		e.eventFiredTime = System.currentTimeMillis();
		e.prevEventOverheadNano = eventOverheadInNano;

		e.id = SWTSocketClient.messageId++;
		e.isDone = false;
		e.type = eventNameMap.get(event.type);
		if (event.widget != null) {
			e.clazz = event.widget.getClass().getName();
			e.listeners = ripListeners(event.widget);
		}

		//
		// 2. Apply event filter
		//
		if (applyFilter(e) == true) {
			doNotSend.add(e.id);
			return e;
		}

		// Set up timers for tracking event handling
		long t1 = System.currentTimeMillis();
		e.eventPrepareTime = t1;
		e.eventHandleTime = t1;

		Shell shell = d.getActiveShell();
		if (event.widget != null && shell != null) {
			Rectangle bounds = getBounds(event.widget);
			if (bounds != null) {
				e.x = bounds.x;
				e.y = bounds.y;
				e.width = bounds.width;
				e.height = bounds.height;
			}

			// On screen
			Widget parent = getParent(event.widget);
			while (parent != null && (parent instanceof Shell == false)) {
				Rectangle rect = getBounds(parent);
				e.x += rect.x;
				e.y += rect.y;
				parent = getParent(parent);
			}

			//
			// Calculate client area location by subtracting border size from shell dimensions
			//
			int clientAreaX = (shell.getSize().x - shell.getClientArea().width) / 2;
			int clientAreaY = shell.getSize().y - shell.getClientArea().height - clientAreaX;
			e.x += clientAreaX;
			e.y += clientAreaY;

			// Capture screenshot
			if (config.isIncludeScreenshots() == true) {
				e.screenShot = recordScreenshot(display, shell);
			}

			//
			// 3. Index in parent - not sure if this is what we need
			//
			event.index = recordIndex(event.widget);
		}

		e.eventPrepareTime = System.currentTimeMillis() - e.eventFiredTime;
		return e;
	}

	private static int recordIndex(Widget w) {
		int index = -1;
		Widget p = getParent(w);
		if (p != null && p instanceof Composite) {
			Control[] children = ((Composite) p).getChildren();
			for (Control child : children) {
				index++;
				if (w == child) {
					break;
				}
			}
		}
		return index;
	}

	private static BufferedImage recordScreenshot(Display d, Shell activeShell) {
		GC gc = new GC(d);
		final Image image = new Image(d, d.getActiveShell().getBounds());
		gc.copyArea(image, activeShell.getBounds().x, activeShell.getBounds().y);
		gc.dispose();
		BufferedImage result = convertToAWT(image.getImageData());
		image.dispose();
		return result;
	}

	private static Rectangle getBounds(Widget w) {
		//
		// 1. Might be a Control itself
		//
		if (w instanceof Control) {
			Control c = (Control) w;
			return c.getBounds();
		}

		//
		// 2. If not try reflection
		//
		try {
			Method m = w.getClass().getDeclaredMethod("getBounds");
			m.setAccessible(true);
			return (Rectangle) m.invoke(w, null);
		} catch (Exception e) {
			// e.printStackTrace();
			return null;
		}
	}

	private static Widget getParent(Widget w) {
		//
		// 1. Might be a Control itself
		//
		if (w instanceof Control) {
			Control c = (Control) w;
			return c.getParent();
		}

		//
		// 2. If not try reflection
		//
		try {
			Method m = w.getClass().getDeclaredMethod("getParent");
			return (Widget) m.invoke(w, null);
		} catch (Exception e) {
			return null;
		}
	}

	public static final EventMessage prepareAndSendMessage(Event e) {
		long overhead = System.nanoTime();

		//
		// 1. Prepare message
		//
		EventMessage em = null;
		try {
			em = prepareMessage(display, e);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		//
		// 2. Check status and send message
		//
		if (em != null && doNotSend.contains(em.id) == false) {
			em = sendMessage(em);
			//
			// The BufferedImage is null'ed so it does not consume memory
			//
			if (em.screenShot != null) {
				em.screenShot.flush();
				em.screenShot = null;
			}

			eventOverheadInNano = System.nanoTime() - overhead;
			return em;
		}
		eventOverheadInNano = System.nanoTime() - overhead;
		return em;
	}

	private static Map<String, String[]> ripListeners(Widget w) throws Exception {
		Map<String, List<String>> listenerMap = new HashMap<String, List<String>>();

		//
		// 1. Retrieve the widget event table
		//
		Object eventTable = null;
		try {
			Field f = Widget.class.getDeclaredField("eventTable");
			f.setAccessible(true);
			eventTable = f.get(w);
		} catch (Exception e) {
			//
			// Event table will be null, nothing to do
			//
		}

		// The "eventTable" field is null if there are no registered event handlers
		if (eventTable == null) {
			return new HashMap<String, String[]>();
		}

		//
		// 2. Retrieve its components
		//
		Field fTypes = eventTable.getClass().getDeclaredField("types");
		fTypes.setAccessible(true);
		int[] types = (int[]) fTypes.get(eventTable);

		Field fListeners = eventTable.getClass().getDeclaredField("listeners");
		fListeners.setAccessible(true);
		Listener[] listeners = (Listener[]) fListeners.get(eventTable);

		//
		// 3. Build temporary Map
		//
		for (int i = 0; i < types.length; i++) {
			if (types[i] == 0) {
				// No more event types
				break;
			}

			//
			// FIXME:
			// There is a bug here due to SWT types/untype listener mechanism. Must fix.
			//
			String swtEventType = getEventHandler(eventNameMap.get(types[i]));
			if (swtEventType != null) {
				if (listenerMap.get(swtEventType) == null) {
					listenerMap.put(swtEventType, new LinkedList<String>());
				}
				listenerMap.get(swtEventType).add(unwrapListener(listeners[i]).getName());
			}
		}

		//
		// 4. Build final version
		//
		Map<String, String[]> returnMap = new HashMap<String, String[]>();
		for (String key : listenerMap.keySet()) {
			if (key != null) {
				returnMap.put(key, (String[]) listenerMap.get(key).toArray(new String[0]));
			}
		}
		return returnMap;
	}

	private static Class unwrapListener(Listener l) {
		if (TypedListener.class.equals(l.getClass()) == false) {
			return l.getClass();
		}

		//
		// If TypedListener, the actual event handler is within its "eventListener" field.
		//
		TypedListener tl = (TypedListener) l;
		return tl.getEventListener().getClass();
	}

	private static BufferedImage convertToAWT(ImageData data) {
		ColorModel colorModel = null;
		PaletteData palette = data.palette;
		if (palette.isDirect) {
			colorModel = new DirectColorModel(data.depth, palette.redMask, palette.greenMask, palette.blueMask);
			BufferedImage bufferedImage = new BufferedImage(colorModel, colorModel.createCompatibleWritableRaster(data.width, data.height), false, null);
			for (int y = 0; y < data.height; y++) {
				for (int x = 0; x < data.width; x++) {
					int pixel = data.getPixel(x, y);
					RGB rgb = palette.getRGB(pixel);
					bufferedImage.setRGB(x, y, rgb.red << 16 | rgb.green << 8 | rgb.blue);
				}
			}
			return bufferedImage;
		} else {
			RGB[] rgbs = palette.getRGBs();
			byte[] red = new byte[rgbs.length];
			byte[] green = new byte[rgbs.length];
			byte[] blue = new byte[rgbs.length];
			for (int i = 0; i < rgbs.length; i++) {
				RGB rgb = rgbs[i];
				red[i] = (byte) rgb.red;
				green[i] = (byte) rgb.green;
				blue[i] = (byte) rgb.blue;
			}
			if (data.transparentPixel != -1) {
				colorModel = new IndexColorModel(data.depth, rgbs.length, red, green, blue, data.transparentPixel);
			} else {
				colorModel = new IndexColorModel(data.depth, rgbs.length, red, green, blue);
			}
			BufferedImage bufferedImage = new BufferedImage(colorModel, colorModel.createCompatibleWritableRaster(data.width, data.height), false, null);
			WritableRaster raster = bufferedImage.getRaster();
			int[] pixelArray = new int[1];
			for (int y = 0; y < data.height; y++) {
				for (int x = 0; x < data.width; x++) {
					int pixel = data.getPixel(x, y);
					pixelArray[0] = pixel;
					raster.setPixel(x, y, pixelArray);
				}
			}
			return bufferedImage;
		}
	}

}