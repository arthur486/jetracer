/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.transform;

import edu.art.module.jetracer.transform.AbstractClassTransformer;

public class EventTableTransformer extends AbstractClassTransformer {

	private final String CLASS_NAME = "org/eclipse/swt/widgets/EventTable";

	private final String METHOD_NAME = "sendEvent(Lorg/eclipse/swt/widgets/Event;)V";

	@Override
	protected boolean isTransformed(String className) {
		return CLASS_NAME.equals(className);
	}

	@Override
	protected String getPreCode(String className, String methodSignature) {
		if (isTransformed(className) == false) {
			throw new IllegalArgumentException(className + " is not transformed.");
		}

		if (METHOD_NAME.equals(methodSignature) == true) {
			return "currentEvent = edu.art.module.jetracer.agent.SWTSocketClient.prepareAndSendMessage($1);";
		}
		return null;
	}

	@Override
	protected String getPostCode(String className, String methodSignature) {
		if (isTransformed(className) == false) {
			throw new IllegalArgumentException(className + " is not transformed.");
		}

		if (METHOD_NAME.equals(methodSignature) == true) {
			return "edu.art.module.jetracer.agent.SWTSocketClient.sendDoneMessage(currentEvent); currentEvent = null;";
		}
		return null;
	}
}
