package edu.art.module.jetracer.agent.transform;

import java.util.HashMap;
import java.util.Map;

import edu.art.module.jetracer.transform.AbstractClassTransformer;

public class SwingClassTransformer extends AbstractClassTransformer {

	// <className,Map<methodSignature,preCode>
	private Map<String, Map<String, String>> transformMap = new HashMap<String, Map<String, String>>();

	private String COMMON_PREFIX = "currentEvent = edu.art.module.jetracer.agent.injected.SwingSocketClient.sendMessage";

	public SwingClassTransformer() {
		buildTransformMap();
	}

	protected void buildTransformMap() {
		//
		// 1. Prepare method maps for instrumented classes
		//
		transformMap.put("javax/swing/AbstractButton", new HashMap<String, String>());
		transformMap.put("javax/swing/AncestorNotifier", new HashMap<String, String>());
		transformMap.put("java/awt/Checkbox", new HashMap<String, String>());
		transformMap.put("java/awt/Component", new HashMap<String, String>());
		transformMap.put("java/awt/Container", new HashMap<String, String>());
		transformMap.put("javax/swing/JComboBox", new HashMap<String, String>());
		transformMap.put("javax/swing/JList", new HashMap<String, String>());
		transformMap.put("javax/swing/JMenuItem", new HashMap<String, String>());
		transformMap.put("javax/swing/JMenu", new HashMap<String, String>());
		transformMap.put("javax/swing/JPopupMenu", new HashMap<String, String>());
		transformMap.put("javax/swing/JSpinner", new HashMap<String, String>());
		transformMap.put("javax/swing/JTabbedPane", new HashMap<String, String>());
		transformMap.put("javax/swing/text/JTextComponent", new HashMap<String, String>());
		transformMap.put("javax/swing/JTree", new HashMap<String, String>());
		transformMap.put("java/awt/Window", new HashMap<String, String>());
		transformMap.put("javax/swing/JInternalFrame", new HashMap<String, String>());

		//
		// 2. Instrument classes for individual events
		//

		//
		// Multiple event calls
		//
		transformMap.get("java/awt/Window").put("processEvent(Ljava/awt/AWTEvent;)V", COMMON_PREFIX + "(this,$1);");
		transformMap.get("java/awt/Container").put("processEvent(Ljava/awt/AWTEvent;)V", COMMON_PREFIX + "(this,$1);");
		transformMap.get("java/awt/Component").put("processEvent(Ljava/awt/AWTEvent;)V", COMMON_PREFIX + "(this,$1);");

		//
		// ActionEvent
		//
		transformMap.get("javax/swing/AbstractButton").put("fireActionPerformed(Ljava/awt/event/ActionEvent;)V",
				COMMON_PREFIX + "(this,\"java.awt.event.ActionEvent\");");

		//
		// ItemEvent
		//
		transformMap.get("javax/swing/AbstractButton").put("fireItemStateChanged(Ljava/awt/event/ItemEvent;)V",
				COMMON_PREFIX + "(this,\"java.awt.event.ItemEvent\");");
		transformMap.get("java/awt/Checkbox").put("processItemEvent(Ljava/awt/event/ItemEvent;)V", COMMON_PREFIX + "(this,\"java.awt.event.ItemEvent\");");
		transformMap.get("javax/swing/JComboBox").put("fireItemStateChanged(Ljava/awt/event/ItemEvent;)V",
				COMMON_PREFIX + "(this,\"java.awt.event.ItemEvent\");");
		transformMap.get("javax/swing/JList").put("processItemEvent(Ljava/awt/event/ItemEvent;)V", COMMON_PREFIX + "(this,\"java.awt.event.ItemEvent\");");

		//
		// CaretEvent
		//
		transformMap.get("javax/swing/text/JTextComponent").put("fireCaretUpdate(Ljavax/swing/event/CaretEvent;)V",
				COMMON_PREFIX + "(this,\"javax.swing.event.CaretEvent\");");

		//
		// ChangeEvent
		//
		transformMap.get("javax/swing/AbstractButton").put("fireStateChanged()V", COMMON_PREFIX + "(this,\"javax.swing.event.ChangeEvent\");");
		transformMap.get("javax/swing/JSpinner").put("fireStateChanged()V", COMMON_PREFIX + "(this,\"javax.swing.event.ChangeEvent\");");
		transformMap.get("javax/swing/JTabbedPane").put("fireStateChanged()V", COMMON_PREFIX + "(this,\"javax.swing.event.ChangeEvent\");");

		//
		// AncestorEvent
		//
		transformMap.get("javax/swing/AncestorNotifier").put("fireAncestorAdded(Ljavax/swing/JComponent;ILjava/awt/Container;Ljava/awt/Container;)V",
				COMMON_PREFIX + "($1,\"javax.swing.event.AncestorEvent.ADDED\");");
		transformMap.get("javax/swing/AncestorNotifier").put("fireAncestorRemoved(Ljavax/swing/JComponent;ILjava/awt/Container;Ljava/awt/Container;)V",
				COMMON_PREFIX + "($1,\"javax.swing.event.AncestorEvent.REMOVED\");");
		transformMap.get("javax/swing/AncestorNotifier").put("fireAncestorMoved(Ljavax/swing/JComponent;ILjava/awt/Container;Ljava/awt/Container;)V",
				COMMON_PREFIX + "($1,\"javax.swing.event.AncestorEvent.MOVED\");");

		//
		// PropertyChangeEvent
		//
		transformMap.get("java/awt/Component").put("firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V",
				COMMON_PREFIX + "(this,\"java.beans.PropertyChangeEvent\");");

		//
		// HierarchyEvent
		//
		transformMap.get("java/awt/Component").put("processHierarchyEvent(Ljava/awt/event/HierarchyEvent;)V",
				COMMON_PREFIX + "(this,\"java.awt.event.HierarchyEvent.HIERARCHY_CHANGED\");");

		//
		// InternalFrameEvent
		//
		transformMap.get("javax/swing/JInternalFrame").put("fireInternalFrameEvent(I)V", COMMON_PREFIX + "($0,$1);");

		//
		// ListSelectionEvent
		//
		transformMap.get("javax/swing/JList").put("fireSelectionValueChanged(IIZ)V", COMMON_PREFIX + "(this,\"javax.swing.event.ListSelectionEvent\");");

		//
		// MenuDragMouseEvent
		//
		transformMap.get("javax/swing/JMenuItem").put("fireMenuDragMouseEntered(Ljavax/swing/event/MenuDragMouseEvent;)V",
				COMMON_PREFIX + "(this,\"javax.swing.event.MenuDragMouseEvent.MENU_DRAG_MOUSE_ENETERED\");");
		transformMap.get("javax/swing/JMenuItem").put("fireMenuDragMouseExited(Ljavax/swing/event/MenuDragMouseEvent;)V",
				COMMON_PREFIX + "(this,\"javax.swing.event.MenuDragMouseEvent.MENU_DRAG_MOUSE_EXITED\");");
		transformMap.get("javax/swing/JMenuItem").put("fireMenuDragMouseDragged(Ljavax/swing/event/MenuDragMouseEvent;)V",
				COMMON_PREFIX + "(this,\"javax.swing.event.MenuDragMouseEvent.MENU_DRAG_MOUSE_DRAGGED\");");
		transformMap.get("javax/swing/JMenuItem").put("fireMenuDragMouseReleased(Ljavax/swing/event/MenuDragMouseEvent;)V",
				COMMON_PREFIX + "(this,\"javax.swing.event.MenuDragMouseEvent.MENU_DRAG_MOUSE_RELEASED\");");

		//
		// MenuEvent
		//
		transformMap.get("javax/swing/JMenu").put("fireMenuSelected()V", COMMON_PREFIX + "(this,\"javax.swing.event.MenuEvent.MENU_SELECTED\");");
		transformMap.get("javax/swing/JMenu").put("fireMenuDeselected()V", COMMON_PREFIX + "(this,\"javax.swing.event.MenuEvent.MENU_DESELECTED\");");
		transformMap.get("javax/swing/JMenu").put("fireMenuCanceled()V", COMMON_PREFIX + "(this,\"javax.swing.event.MenuEvent.MENU_CANCELED\");");

		//
		// MenuKeyEvent
		//
		transformMap.get("javax/swing/JMenuItem").put("fireMenuKeyPressed(Ljavax/swing/event/MenuKeyEvent;)V",
				COMMON_PREFIX + "(this,\"javax.swing.event.MenuKeyEvent.MENU_KEY_PRESSED\");");
		transformMap.get("javax/swing/JMenuItem").put("fireMenuKeyReleased(Ljavax/swing/event/MenuKeyEvent;)V",
				COMMON_PREFIX + "(this,\"javax.swing.event.MenuKeyEvent.MENU_KEY_RELEASED\");");
		transformMap.get("javax/swing/JMenuItem").put("fireMenuKeyTyped(Ljavax/swing/event/MenuKeyEvent;)V",
				COMMON_PREFIX + "(this,\"javax.swing.event.MenuKeyEvent.MENU_KEY_TYPED\");");

		//
		// PopupMenuEvent
		//
		transformMap.get("javax/swing/JPopupMenu").put("firePopupMenuWillBecomeVisible()V",
				COMMON_PREFIX + "(this,\"javax.swing.event.PopupMenuEvent.WILL_BECOME_VISIBLE\");");
		transformMap.get("javax/swing/JPopupMenu").put("firePopupMenuWillBecomeInvisible()V",
				COMMON_PREFIX + "(this,\"javax.swing.event.PopupMenuEvent.WILL_BECOME_INVISIBLE\");");
		transformMap.get("javax/swing/JPopupMenu").put("firePopupMenuCanceled()V", COMMON_PREFIX + "(this,\"javax.swing.event.PopupMenuEvent.CANCELED\");");

		//
		// TreeExpansionEvent
		//
		transformMap.get("javax/swing/JTree").put("fireTreeExpanded(Ljavax/swing/tree/TreePath;)V",
				COMMON_PREFIX + "(this,\"javax.swing.event.TreeExpansionEvent.EXPANDED\");");
		transformMap.get("javax/swing/JTree").put("fireTreeCollapsed(Ljavax/swing/tree/TreePath;)V",
				COMMON_PREFIX + "(this,\"javax.swing.event.TreeExpansionEvent.COLLAPSED\");");
		transformMap.get("javax/swing/JTree").put("fireTreeWillExpand(Ljavax/swing/tree/TreePath;)V",
				COMMON_PREFIX + "(this,\"javax.swing.event.TreeExpansionEvent.WILL_EXPAND\");");
		transformMap.get("javax/swing/JTree").put("fireTreeWillCollapse(Ljavax/swing/tree/TreePath;)V",
				COMMON_PREFIX + "(this,\"javax.swing.event.TreeExpansionEvent.WILL_COLLAPSE\");");

		//
		// TreeSelectionEvent
		//
		transformMap.get("javax/swing/JTree").put("fireValueChanged(Ljavax/swing/event/TreeSelectionEvent;)V",
				COMMON_PREFIX + "(this,\"javax.swing.event.TreeSelectionEvent\");");
	}

	protected boolean isTransformed(String className) {
		return transformMap.keySet().contains(className);
	};

	protected String getPreCode(String className, String methodSignature) {
		if (isTransformed(className) == false) {
			throw new IllegalArgumentException(className + " is not transformed.");
		}
		return transformMap.get(className).get(methodSignature);
	}

	protected String getPostCode(String className, String methodSignature) {
		if (isTransformed(className) == false) {
			throw new IllegalArgumentException(className + " is not transformed.");
		}

		// Only add post-code to instrumented methods
		if (getPreCode(className, methodSignature) != null) {
			return "edu.art.module.jetracer.agent.injected.SwingSocketClient.sendDoneMessage(currentEvent); currentEvent = null;";
		}
		return null;
	}
}
