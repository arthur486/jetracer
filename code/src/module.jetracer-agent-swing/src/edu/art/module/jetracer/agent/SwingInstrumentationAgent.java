/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.agent;

import java.lang.instrument.Instrumentation;

import edu.art.module.jetracer.agent.transform.SwingClassTransformer;

public class SwingInstrumentationAgent {

	public static void premain(String agentArgs, Instrumentation inst) {
		inst.addTransformer(new SwingClassTransformer());
	}
}