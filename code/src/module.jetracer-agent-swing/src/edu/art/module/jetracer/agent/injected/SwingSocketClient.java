/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.agent.injected;

import java.awt.AWTEvent;
import java.awt.AWTException;
import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Window;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPopupMenu;

import edu.art.module.jetracer.agent.DefaultSocketClient;
import edu.art.module.jetracer.common.EventMessage;

public class SwingSocketClient extends DefaultSocketClient {

	private static int messageId = 1;

	private static Map<Integer, String> messageMap = null;

	private static Robot robot = null;

	protected static Map<String, String[]> ripListeners(Component c) throws Exception {
		Map<String, String[]> listenerMap = new HashMap<String, String[]>();
		for (Method met : c.getClass().getMethods()) {
			//
			// 1. Check getXXXListeners() signature
			//
			if (met.getName().startsWith("get") == false || met.getName().endsWith("Listeners") == false || met.getParameterTypes().length != 0) {
				continue;
			}
			/*
			 * We assume the getXXXListeners() style methods use the EventListenerList's getListeners(...) method, such as:
			 * 
			 * public ActionListener[] getActionListeners() { return (ActionListener[])(listenerList.getListeners(ActionListener.class)); }
			 * 
			 * This assumption is important for keeping the event listeners' order consistent. (listener order in returned array must match their execution
			 * order, last added listener is executed first)
			 */
			String listenerType = met.getName().substring(3, met.getName().length() - 1);
			EventListener[] listeners = (EventListener[]) met.invoke(c, new Object[] {});

			// Only consider existing listeners
			if (listeners.length > 0) {
				String[] lNames = new String[listeners.length];
				for (int i = 0; i < listeners.length; i++) {
					lNames[i] = listeners[i].getClass().getName();
				}

				listenerMap.put(listenerType, lNames);
			}
		}
		return listenerMap;
	}

	private static Component getContainerParent(Component c) {
		if (c == null) {
			return null;
		}
		if (c instanceof JPopupMenu) {
			return ((JPopupMenu) c).getInvoker();
		}
		return c.getParent();
	}

	private static Window getContainingWindow(Component c) {

		while (c != null && c instanceof Window == false) {
			c = getContainerParent(c);
		}
		return (Window) c;
	}

	private static boolean hasDesiredLocation(Component c) {
		try {
			c.getClass().getDeclaredField("desiredLocationX");
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private static int getDesiredX(Component c) {
		Field f = null;
		try {
			f = c.getClass().getDeclaredField("desiredLocationX");
			f.setAccessible(true);
			return (Integer) f.get(c);
		} catch (Exception e) {
			return Integer.MIN_VALUE;
		}
	}

	private static int getDesiredY(Component c) {
		Field f = null;
		try {
			f = c.getClass().getDeclaredField("desiredLocationY");
			f.setAccessible(true);
			return (Integer) f.get(c);
		} catch (Exception e) {
			return Integer.MIN_VALUE;
		}
	}

	protected static boolean applyFilter(EventMessage em) {
		return applyListenerFilter("Swing", em) || applyEventFilter(em);
	}

	protected static EventMessage prepareMessage(Component c, String eventType) throws Exception {
		//
		// 1. Initialize message object
		//
		EventMessage e = new EventMessage();
		e.eventFiredTime = System.currentTimeMillis();
		e.prevEventOverheadNano = eventOverheadInNano;

		e.id = SwingSocketClient.messageId++;
		e.isDone = false;
		e.type = eventType;
		e.clazz = c.getClass().getName();
		e.listeners = ripListeners(c);

		//
		// 2. Apply event filter
		//
		if (applyFilter(e) == true) {
			doNotSend.add(e.id);
			return e;
		}

		//
		// 3. Record other parameters
		//
		if (config.isIncludeScreenshots() == true) {
			e.screenShot = recordScreenshot(c);
		}
		e.index = recordIndex(c);
		Point location = recordLocation(c);
		e.x = location.x;
		e.y = location.y;
		e.width = c.getWidth();
		e.height = c.getHeight();

		e.eventPrepareTime = System.currentTimeMillis() - e.eventFiredTime;
		return e;
	}

	private static Robot getAWTRobot() {
		if (robot == null) {
			try {
				robot = new Robot();
			} catch (AWTException e) {
				e.printStackTrace();
				return null;
			}
		}
		return robot;
	}

	private static BufferedImage recordScreenshot(Component c) {
		Window parentWindow = getContainingWindow(c);
		if (parentWindow != null && parentWindow.getSize().width > 0 && parentWindow.getSize().height > 0) {
			java.awt.Rectangle captureSize = new java.awt.Rectangle(parentWindow.getLocation(), parentWindow.getSize());
			return getAWTRobot().createScreenCapture(captureSize);
		}
		return null;
	}

	private static int recordIndex(Component c) {
		synchronized (c.getTreeLock()) {
			Container cc = (Container) getContainerParent(c);

			// cc == null at GUI init
			if (cc != null) {
				for (int i = 0; i < cc.getComponents().length; i++) {
					if (cc.getComponent(i) == c) {
						return i;
					}
				}
			}
		}
		return -1;
	}

	private static Point recordLocation(Component c) {
		Point location = new Point(0, 0);
		Component pointer = getContainerParent(c);

		if (c.isShowing() == true) {
			location.x = c.getX();
			location.y = c.getY();
			while (pointer != null && pointer instanceof Window == false) {
				location.x += pointer.getX();
				location.y += pointer.getY();
				pointer = getContainerParent(pointer);
			}
		}

		if (c.isShowing() == false) {
			pointer = c;
			location.x = 0;
			location.y = 0;

			while (pointer != null && pointer instanceof Window == false) {
				if (hasDesiredLocation(pointer)) {
					location.x += getDesiredX(pointer);
					location.y += getDesiredY(pointer);

					// Compensate with window location if it can be determined
					Window w = getContainingWindow(c);
					if (w != null && w.isShowing() == true) {
						Point pt = w.getLocationOnScreen();
						location.x -= pt.x;
						location.y -= pt.y;
					}
					break;
				} else {
					location.x += pointer.getX();
					location.y += pointer.getY();
					pointer = getContainerParent(pointer);
				}
			}
		}
		return location;
	}

	public static final String translateEventMessage(int id) {
		if (messageMap == null) {
			messageMap = new HashMap<Integer, String>();
			messageMap.put(java.awt.event.FocusEvent.FOCUS_GAINED, "java.awt.event.FocusEvent.FOCUS_GAINED");
			messageMap.put(java.awt.event.FocusEvent.FOCUS_LOST, "java.awt.event.FocusEvent.FOCUS_LOST");
			messageMap.put(java.awt.event.MouseEvent.MOUSE_PRESSED, "java.awt.event.MouseEvent.MOUSE_PRESSED");
			messageMap.put(java.awt.event.MouseEvent.MOUSE_RELEASED, "java.awt.event.MouseEvent.MOUSE_RELEASED");
			messageMap.put(java.awt.event.MouseEvent.MOUSE_CLICKED, "java.awt.event.MouseEvent.MOUSE_CLICKED");
			messageMap.put(java.awt.event.MouseEvent.MOUSE_ENTERED, "java.awt.event.MouseEvent.MOUSE_ENTERED");
			messageMap.put(java.awt.event.MouseEvent.MOUSE_EXITED, "java.awt.event.MouseEvent.MOUSE_EXITED");
			messageMap.put(java.awt.event.MouseEvent.MOUSE_MOVED, "java.awt.event.MouseEvent.MOUSE_MOVED");
			messageMap.put(java.awt.event.MouseEvent.MOUSE_DRAGGED, "java.awt.event.MouseEvent.MOUSE_DRAGGED");
			messageMap.put(java.awt.event.MouseEvent.MOUSE_WHEEL, "java.awt.event.MouseEvent.MOUSE_WHEEL");
			messageMap.put(java.awt.event.KeyEvent.KEY_TYPED, "java.awt.event.KeyEvent.KEY_TYPED");
			messageMap.put(java.awt.event.KeyEvent.KEY_PRESSED, "java.awt.event.KeyEvent.KEY_PRESSED");
			messageMap.put(java.awt.event.KeyEvent.KEY_RELEASED, "java.awt.event.KeyEvent.KEY_RELEASED");
			messageMap.put(java.awt.event.ComponentEvent.COMPONENT_RESIZED, "java.awt.event.ComponentEvent.COMPONENT_RESIZED");
			messageMap.put(java.awt.event.ComponentEvent.COMPONENT_MOVED, "java.awt.event.ComponentEvent.COMPONENT_MOVED");
			messageMap.put(java.awt.event.ComponentEvent.COMPONENT_SHOWN, "java.awt.event.ComponentEvent.COMPONENT_SHOWN");
			messageMap.put(java.awt.event.ComponentEvent.COMPONENT_HIDDEN, "java.awt.event.ComponentEvent.COMPONENT_HIDDEN");
			messageMap.put(java.awt.event.HierarchyEvent.HIERARCHY_CHANGED, "java.awt.event.HierarchyEvent.HIERARCHY_CHANGED");
			messageMap.put(java.awt.event.HierarchyEvent.ANCESTOR_MOVED, "java.awt.event.HierarchyEvent.ANCESTOR_MOVED");
			messageMap.put(java.awt.event.HierarchyEvent.ANCESTOR_RESIZED, "java.awt.event.HierarchyEvent.ANCESTOR_RESIZED");
			messageMap.put(java.awt.event.InputMethodEvent.INPUT_METHOD_TEXT_CHANGED, "java.awt.event.InputMethodEvent.INPUT_METHOD_TEXT_CHANGED");
			messageMap.put(java.awt.event.InputMethodEvent.CARET_POSITION_CHANGED, "java.awt.event.InputMethodEvent.CARET_POSITION_CHANGED");
			messageMap.put(java.awt.event.WindowEvent.WINDOW_OPENED, "java.awt.event.WindowEvent.WINDOW_OPENED");
			messageMap.put(java.awt.event.WindowEvent.WINDOW_CLOSING, "java.awt.event.WindowEvent.WINDOW_CLOSING");
			messageMap.put(java.awt.event.WindowEvent.WINDOW_CLOSED, "java.awt.event.WindowEvent.WINDOW_CLOSED");
			messageMap.put(java.awt.event.WindowEvent.WINDOW_ICONIFIED, "java.awt.event.WindowEvent.WINDOW_ICONIFIED");
			messageMap.put(java.awt.event.WindowEvent.WINDOW_DEICONIFIED, "java.awt.event.WindowEvent.WINDOW_DEICONIFIED");
			messageMap.put(java.awt.event.WindowEvent.WINDOW_ACTIVATED, "java.awt.event.WindowEvent.WINDOW_ACTIVATED");
			messageMap.put(java.awt.event.WindowEvent.WINDOW_DEACTIVATED, "java.awt.event.WindowEvent.WINDOW_DEACTIVATED");
			messageMap.put(java.awt.event.WindowEvent.WINDOW_GAINED_FOCUS, "java.awt.event.WindowEvent.WINDOW_GAINED_FOCUS");
			messageMap.put(java.awt.event.WindowEvent.WINDOW_LOST_FOCUS, "java.awt.event.WindowEvent.WINDOW_LOST_FOCUS");
			messageMap.put(java.awt.event.WindowEvent.WINDOW_STATE_CHANGED, "java.awt.event.WindowEvent.WINDOW_STATE_CHANGED");
			messageMap.put(java.awt.event.ContainerEvent.COMPONENT_ADDED, "java.awt.event.ContainerEvent.COMPONENT_ADDED");
			messageMap.put(java.awt.event.ContainerEvent.COMPONENT_REMOVED, "java.awt.event.ContainerEvent.COMPONENT_REMOVED");
			messageMap.put(javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_OPENED, "javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_OPENED");
			messageMap.put(javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_CLOSING, "javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_CLOSING");
			messageMap.put(javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_CLOSED, "javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_CLOSED");
			messageMap.put(javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_ICONIFIED, "javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_ICONIFIED");
			messageMap.put(javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_DEICONIFIED, "javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_DEICONIFIED");
			messageMap.put(javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_ACTIVATED, "javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_ACTIVATED");
			messageMap.put(javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_DEACTIVATED, "javax.swing.event.InternalFrameEvent.INTERNAL_FRAME_DEACTIVATED");
		}
		return messageMap.get(id);
	}

	//
	// These work for most AWT/Swing events
	//
	public static final EventMessage sendMessage(Component c, AWTEvent e) {
		return prepareAndSendMessage(c, translateEventMessage(e.getID()));
	}

	public static final EventMessage sendMessage(Component c, String s) {
		return prepareAndSendMessage(c, s);
	}

	//
	// JInternalFrame
	//
	public static final EventMessage sendMessage(Component c, int messageId) {
		return prepareAndSendMessage(c, translateEventMessage(messageId));
	}

	public static final EventMessage prepareAndSendMessage(Component c, String e) {
		long overhead = System.nanoTime();

		//
		// 1. Prepare message
		//
		EventMessage em = null;
		try {
			em = prepareMessage(c, e);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		//
		// 2. Check status and send message
		//
		if (em != null && doNotSend.contains(em.id) == false) {
			em = sendMessage(em);
			
			//
			// The BufferedImage is null'ed so it does not consume memory
			//
			if (em.screenShot != null) {
				em.screenShot.flush();
				em.screenShot = null;
			}
			eventOverheadInNano = System.nanoTime() - overhead;
			return em;
		}
		eventOverheadInNano = System.nanoTime() - overhead;
		return em;
	}
}
