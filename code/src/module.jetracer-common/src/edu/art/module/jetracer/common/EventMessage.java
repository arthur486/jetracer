/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.common;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public class EventMessage implements Serializable {

	@XmlTransient
	private static final long serialVersionUID = -4640865512851468428L;

	/**
	 * Each EventMessage should have a unique assigned id
	 */
	public int id = -1;

	/**
	 * isDone == false when event handling is underway, and 'true' when done
	 */
	public boolean isDone = false;

	/**
	 * Used solely for debug purposes
	 */
	@XmlTransient
	public String debug = "";

	/**
	 * Properties of the widget triggering the message
	 */
	public String clazz = null;
	public int index = -1;
	public int x = -1;
	public int y = -1;
	public int width = -1;
	public int height = -1;

	/**
	 * Recorded screenshot of application window
	 */
	public transient BufferedImage screenShot = null;

	/**
	 * Event time in target VM
	 */
	public long eventFiredTime = -1;

	/**
	 * How long it took to build this EventMessage instance (milis).
	 */
	public long eventPrepareTime = -1;

	/**
	 * How long it took to handle this event (milis).
	 */
	public long eventHandleTime = -1;

	/**
	 * Total overhead of JETracer (microseconds).
	 */
	public long prevEventOverheadNano = 0;

	/**
	 * The type of the event as the name of the handler interface (e.g. 'java.awt.event.ActionListener')
	 */
	public String type = null;

	/**
	 * The map of event listener classes (e.g. key=ActionListeners, values={A,B,C})
	 */
	public Map<String, String[]> listeners = null;

	/**
	 * Set iff an error occurs while building message
	 */
	@XmlTransient
	public Exception error = null;

	public EventMessage() {
	}

	@Override
	public String toString() {
		String s1 = "id=" + id + ";isDone=" + isDone + "\n";
		s1 += "Class=" + clazz + "\n";
		s1 += "Index=" + index + "\n";
		s1 += "(X,Y,Width,Height)=(" + x + "," + y + "," + width + "," + height + ")\n";
		s1 += "Event message prepare time=" + eventPrepareTime + "\n";
		s1 += "Event handle time=" + eventHandleTime + "\n";
		s1 += (screenShot != null ? "Screenshot size=" + screenShot.getWidth() + "x" + screenShot.getHeight() + "\n" : "");
		s1 += "Event type=" + type + "\n";

		if (listeners != null && listeners.keySet().size() > 0) {
			String s2 = "\nListeners:\n";

			for (String k : listeners.keySet()) {
				s2 += "\t" + k + "\n";
				for (String v : listeners.get(k)) {
					s2 += "\t\t" + v + "\n";
				}
			}
			s1 = s1 + s2;
		}

		if (error != null) {
			s1 += "\nError:\n";
			s1 += error.getMessage();
		}
		// s1 = s1 + "\nDebug:\n" + debug;

		return s1;
	}

	private void writeObject(ObjectOutputStream out) throws IOException {
		//
		// 1. Call default write
		//
		out.defaultWriteObject();

		//
		// 2. Handle the screenshot
		//
		out.writeBoolean((screenShot != null ? true : false));
		if (screenShot != null) {
			ImageIO.write(screenShot, "png", out);
		}
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		boolean flag = in.readBoolean();
		if (flag == true) {
			screenShot = ImageIO.read(in);
		}
	}
}
