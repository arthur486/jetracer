/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.handlers;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class Event {

	private String name;
	private List<String> handlerMethods;

	public Event() {
	}

	@XmlAttribute
	public String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "handlerMethod")
	public List<String> getHandlerMethods() {
		return handlerMethods;
	}

	protected void setHandlerMethods(List<String> handlerMethods) {
		this.handlerMethods = handlerMethods;
	}
}
