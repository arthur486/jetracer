/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.agent;

import java.io.File;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import edu.art.module.jetracer.EventGranularity;

public class InstrumentConfig implements Serializable {

	private static final long serialVersionUID = -7310118611628222991L;

	private Set<String> applicationPackages = new HashSet<String>();

	private EventGranularity eventGranularity = EventGranularity.All;

	private Set<String> filteredEvents = new HashSet<String>();

	private File output;

	private int port = 6000;

	private boolean includeScreenshots = false;

	public InstrumentConfig() {
	}

	public InstrumentConfig(Set<String> applicationPackages, EventGranularity eventGranularity, Set<String> filteredEvents, boolean includeScreenshots,
			File output) {
		if (applicationPackages != null) {
			this.applicationPackages.addAll(applicationPackages);
		}
		this.eventGranularity = eventGranularity;
		if (filteredEvents != null) {
			this.filteredEvents.addAll(filteredEvents);
		}
		this.includeScreenshots = includeScreenshots;
		this.output = output;
	}

	public InstrumentConfig(Set<String> applicationPackages, EventGranularity eventGranularity, Set<String> filteredEvents, boolean includeScreenshots,
			File output, int port) {
		this(applicationPackages, eventGranularity, filteredEvents, includeScreenshots, output);
		this.port = port;
	}

	public void setApplicationPackages(Set<String> applicationPackages) {
		if (applicationPackages != null) {
			this.applicationPackages = applicationPackages;
		} else {
			this.applicationPackages.clear();
		}
	}

	public Set<String> getApplicationPackages() {
		return applicationPackages;
	}

	public void setEventGranularity(EventGranularity eventGranularity) {
		this.eventGranularity = eventGranularity;
	}

	public EventGranularity getEventGranularity() {
		return eventGranularity;
	}

	public void setFilteredEvents(Set<String> filteredEvents) {
		if (filteredEvents != null) {
			this.filteredEvents = filteredEvents;
		} else {
			this.filteredEvents.clear();
		}
	}

	public Set<String> getFilteredEvents() {
		return filteredEvents;
	}

	public void setIncludeScreenshots(boolean includeScreenshots) {
		this.includeScreenshots = includeScreenshots;
	}

	public boolean isIncludeScreenshots() {
		return includeScreenshots;
	}

	public void setOutput(File output) {
		this.output = output;
	}

	public File getOutput() {
		return output;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getPort() {
		return port;
	}

	@Override
	public String toString() {
		return new String("Application packages: " + applicationPackages + ", Event Granularity: " + eventGranularity + ", Filtered Events: " + filteredEvents
				+ ", include widget screenshots: " + includeScreenshots + ", port: " + port);
	}
}
