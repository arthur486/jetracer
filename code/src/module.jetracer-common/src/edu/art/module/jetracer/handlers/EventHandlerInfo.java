/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.handlers;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "eventHandlerInfo")
public class EventHandlerInfo {

	private List<EventHandlerBundle> bundles;

	public EventHandlerInfo() {
	}

	@XmlElement(name = "eventHandlerBundle")
	public List<EventHandlerBundle> getBundles() {
		return bundles;
	}

	protected void setBundles(List<EventHandlerBundle> bundles) {
		this.bundles = bundles;
	}

	public EventHandlerBundle getBundle(String name) {
		if (name == null) {
			throw new IllegalArgumentException();
		}

		for (EventHandlerBundle bundle : getBundles()) {
			if (name.equals(bundle.getName())) {
				return bundle;
			}
		}
		return null;
	}
}
