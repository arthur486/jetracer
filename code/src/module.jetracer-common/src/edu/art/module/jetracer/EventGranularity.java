package edu.art.module.jetracer;

public enum EventGranularity {
	All, AnyListener, ActionedListener
}
