/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.handlers;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

public class HandlerInfoService {

	private static EventHandlerInfo handlerInfo;

	public static final EventHandlerInfo getHandlerInfo() {
		if (handlerInfo == null) {
			handlerInfo = (EventHandlerInfo) unmarshall(EventHandlerInfo.class.getResourceAsStream("/edu/art/module/jetracer/handlers/HandlerInfo.xml"),
					EventHandlerInfo.class);
		}
		return handlerInfo;
	}

	private static synchronized Object unmarshall(InputStream inputStream, Class<EventHandlerInfo> clazz) {
		try {
			Unmarshaller unmarshaller = JAXBContext.newInstance(clazz).createUnmarshaller();
			return unmarshaller.unmarshal(inputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
