/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.transform;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;

public abstract class AbstractClassTransformer implements ClassFileTransformer {

	protected final String EVENT_TYPE_CONSTANT = "%EVENT_TYPE%";

	private final Logger logger = Logger.getLogger(AbstractClassTransformer.class.getName());

	private FileHandler fHandler = null;

	public AbstractClassTransformer() {

		try {
			this.fHandler = new FileHandler("instrumentation.log");
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.fHandler.setFormatter(new OneLineFormatter());
		this.logger.addHandler(fHandler);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public byte[] transform(ClassLoader loader, String className, Class classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer)
			throws IllegalClassFormatException {
		/*
		 * String tClassName = getTransformedClassName(); if (tClassName.equals(className) == false) { return classfileBuffer; }
		 */
		try {

			if (isTransformed(className) == false) {
				return null;
			}

			logger.info("Instrumenting class - " + className);

			ClassPool cp = ClassPool.getDefault();
			CtClass cc = cp.get(className.replaceAll("/", "."));

			for (CtMethod m : cc.getDeclaredMethods()) {
				if (transformByteCode(className, m) == true) {
					logger.info("Instrumenting method - " + m.getName() + m.getSignature());
				}
			}

			byte[] byteCode = cc.toBytecode();
			cc.detach();

			return byteCode;
		} catch (Throwable ex) {
			ex.printStackTrace();
			System.out.println("ERROR\n" + ex.getMessage());
			logger.warning(ex.getMessage());
		}
		return null;
	}

	protected boolean transformByteCode(String className, CtMethod method) throws CannotCompileException {
		String preCode = getPreCode(className, method.getName() + method.getSignature());
		String postCode = getPostCode(className, method.getName() + method.getSignature());

		if (preCode == null && postCode == null) {
			return false;
		}

		//
		// 1. Inject variable holding currently handled event
		//
		try {
			method.addLocalVariable("currentEvent", ClassPool.getDefault().get("edu.art.module.jetracer.common.EventMessage"));
		} catch (NotFoundException e) {
			e.printStackTrace();
		}

		//
		// 2. Inject instrumentation code
		//
		method.insertBefore(preCode);
		method.insertAfter(postCode);
		return true;
	}

	protected boolean isTransformed(String className) {
		return false;
	};

	protected String getPreCode(String className, String methodSignature) {
		return null;
	}

	protected String getPostCode(String className, String methodSignature) {
		return null;
	}
}