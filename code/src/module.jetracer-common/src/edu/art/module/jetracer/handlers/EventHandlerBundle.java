/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.handlers;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class EventHandlerBundle {

	private String name;

	private List<EventHandler> eventHandlers;

	public EventHandlerBundle() {
	}

	@XmlAttribute
	public String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "eventHandler")
	public List<EventHandler> getEventHandlers() {
		return eventHandlers;
	}

	protected void setEventHandlers(List<EventHandler> eventHandlers) {
		this.eventHandlers = eventHandlers;
	}

	public EventHandler findEventHandler(String eventName) {
		if (eventName == null) {
			throw new IllegalArgumentException();
		}

		for (EventHandler eh : getEventHandlers()) {
			for (Event e : eh.getEvents()) {
				if (eventName.equals(e.getName())) {
					return eh;
				}
			}
		}
		return null;
	}
}
