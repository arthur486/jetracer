/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.handlers;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class EventHandler {

	private String name;

	private List<Event> events;

	public EventHandler() {
	}

	@XmlAttribute
	public String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	@XmlElement(name = "event")
	public List<Event> getEvents() {
		return events;
	}

	public Event getEvent(String name) {
		if (name == null) {
			throw new IllegalArgumentException();
		}

		for (Event e : getEvents()) {
			if (e.getName().equals(name) == true) {
				return e;
			}
		}
		return null;
	}
}
