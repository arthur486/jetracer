/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.agent;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.art.module.jetracer.EventGranularity;
import edu.art.module.jetracer.common.EventMessage;
import edu.art.module.jetracer.handlers.EventHandler;
import edu.art.module.jetracer.handlers.HandlerInfoService;

public class DefaultSocketClient {

	protected static InstrumentConfig config = null;

	protected static Socket socket = null;

	private static int port = 6000;

	protected static ObjectOutputStream out = null;

	protected static Set<Integer> doNotSend = new HashSet<Integer>();

	protected static long lastSocketTime = 0;

	protected static long eventOverheadInNano = 0;

	static {
		receiveInitData();
	}

	protected static String[] filterActionedListeners(String guiBundle, EventMessage e) {
		EventHandler handler = HandlerInfoService.getHandlerInfo().getBundle(guiBundle).findEventHandler(e.type);
		if (handler == null) {
			throw new RuntimeException("Cannot find handler for event type: " + e.type);
		}
		String[] lNames = e.listeners.get(handler.getName());

		// No handlers for triggered event type
		if (lNames == null || lNames.length == 0) {
			return new String[] {};
		}

		String[] result = new String[lNames.length];
		int i = 0;
		for (String name : lNames) {
			String pName = name.substring(0, name.lastIndexOf('.'));

			if (isApplicationPackage(pName)) {
				result[i++] = name;
			}

			// if (config.getApplicationPackages().contains(pName)) {
			// result[i++] = name;
			// }
		}
		return Arrays.copyOf(result, i);
	}

	protected static String[] filterApplicationClasses(String[] classNames) {
		String[] temp = new String[classNames.length];
		int i = 0;

		for (String s : classNames) {
			String pName = s.substring(0, s.lastIndexOf('.'));
			if (config.getApplicationPackages().contains(pName) == true) {
				temp[i++] = s;
			}
		}
		return Arrays.copyOf(temp, i);
	}

	protected static Map<String, String[]> filterApplicationListeners(EventMessage e) {
		Map<String, String[]> result = new HashMap<String, String[]>();

		for (String listenerType : e.listeners.keySet()) {
			String[] appListeners = filterApplicationClasses(e.listeners.get(listenerType));
			if (appListeners.length > 0) {
				result.put(listenerType, appListeners);
			}
		}

		return result;
	}

	protected static final boolean isApplicationPackage(String packageName) {
		for (String appPackageName : config.getApplicationPackages()) {
			if (packageName.startsWith(appPackageName) == true) {
				return true;
			}
		}
		return false;
	}

	protected static boolean applyListenerFilter(String guiBundle, EventMessage e) {
		if (config.getApplicationPackages() == null) {
			return false;
		}

		//
		// EventGranularity.All - send all event messages
		//
		if (EventGranularity.All == config.getEventGranularity()) {
			return false;
		}

		//
		// EventGranularity.AnyListener - send event message if it has at least one application listener
		//
		if (EventGranularity.AnyListener == config.getEventGranularity()) {
			return filterApplicationListeners(e).size() == 0;
		}

		//
		// EventGranularity.ActionedListener - send event message only if application listener is actioned
		//
		if (EventGranularity.ActionedListener == config.getEventGranularity()) {
			return filterActionedListeners(guiBundle, e).length == 0;
		}
		return true;
	}

	protected static boolean applyEventFilter(EventMessage em) {
		for (String excludedEvent : config.getFilteredEvents()) {
			if (em.type.indexOf(excludedEvent) >= 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param listenerMap
	 *            The listener map of the event
	 * @return True iif there exists at least one application defined listener (it does not have to be a listener for the current event)
	 */
	protected static boolean includesApplicationListener(Map<String, String[]> listenerMap) {
		for (String[] lType : listenerMap.values()) {
			for (String lName : lType) {
				String pName = lName.substring(0, lName.lastIndexOf('.'));
				if (config.getApplicationPackages().contains(pName) == true) {
					return true;
				}
			}
		}
		return false;
	}

	public static final EventMessage sendMessage(EventMessage em) {
		internalSendMessage(em);
		return em;
	}

	protected static void receiveInitData() {
		if (out == null) {
			try {
				socket = new Socket("localhost", port);
				out = new ObjectOutputStream(socket.getOutputStream());
				ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
				config = (InstrumentConfig) in.readObject();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	protected static final synchronized void internalSendMessage(EventMessage e) {
		try {
			out.writeObject(e);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public static final void sendDoneMessage(EventMessage e) {
		long overhead = System.nanoTime();

		if (doNotSend.contains(e.id) == true) {
			doNotSend.remove(e.id);
			return;
		}

		EventMessage em = new EventMessage();
		em.id = e.id;
		em.isDone = true;
		em.prevEventOverheadNano = eventOverheadInNano;
		internalSendMessage(em);

		eventOverheadInNano = System.nanoTime() - overhead;
	}
}