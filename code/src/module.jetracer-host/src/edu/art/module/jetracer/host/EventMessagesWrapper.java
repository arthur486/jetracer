/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.host;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import edu.art.module.jetracer.common.EventMessage;

@XmlRootElement(name = "events")
public class EventMessagesWrapper {
	@XmlElement(name = "event")
	public List<EventMessage> events = new LinkedList<EventMessage>();
}
