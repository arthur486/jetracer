/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.host;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.imageio.ImageIO;
import javax.swing.event.EventListenerList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;

import edu.art.module.jetracer.agent.InstrumentConfig;
import edu.art.module.jetracer.common.EventMessage;
import edu.art.module.jetracer.common.ExitEventMessage;
import edu.art.module.jetracer.host.exception.InstrumentationException;
import edu.art.module.jetracer.host.exception.NetworkException;
import edu.art.module.jetracer.host.server.event.EventMessageListener;
import edu.art.module.jetracer.host.server.event.EventMessageReceivedEvent;

public class WidgetInstrumentationConnector implements Runnable {

	private FileWriter outputWriter = null;

	private EventMessagesWrapper eventsWrapper = new EventMessagesWrapper();

	private Marshaller marshaller = null;

	private Thread thread = null;

	private ServerSocket socket = null;

	private Socket clientSocket = null;

	private ObjectInputStream in = null;

	private InstrumentConfig config;

	private File screenshotDir;

	private EventListenerList listenerList = new EventListenerList();

	private EventMessageReceivedEvent event = null;

	protected boolean isConnected = false;

	//
	// Attributes below are used for performance benchmark
	//
	private long totalOverheadInNano = 0;

	private long eventCount = 0;

	NumberFormat formatter = new DecimalFormat("#.##");

	public WidgetInstrumentationConnector(InstrumentConfig config) {
		this.config = config;

		if (config.getOutput() != null) {
			this.screenshotDir = new File(config.getOutput().getParent());
			try {
				outputWriter = new FileWriter(config.getOutput());
				marshaller = JAXBContext.newInstance(EventMessagesWrapper.class).createMarshaller();
				marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			} catch (IOException e) {
				throw new InstrumentationException(e);
			} catch (JAXBException e) {
				throw new InstrumentationException(e);
			}
		}
	}

	protected void checkConnection() {
		if (isConnected == false) {
			String msg = "Connection to application not established";
			Logger.getLogger(getClass()).error(msg);
			throw new NetworkException(msg);
		}
	}

	public void connect() {
		Logger.getLogger(getClass()).info("Waiting for connection...");
		thread = new Thread(this);
		thread.start();
	}

	public void disconnect() {
		//
		// Disconnect
		//
		Logger.getLogger(getClass()).info("Disconnecting...");
		isConnected = false;
		try {
			clientSocket.close();
			socket.close();
		} catch (IOException e1) {
			throw new NetworkException(e1);
		}

		//
		// Show performance benchmark
		//
		Logger.getLogger(getClass()).info("Event instrumentation performance info:");
		Logger.getLogger(getClass()).info("Total event instrumentation time: " + formatter.format((double) totalOverheadInNano / 1000000) + " milis.");
		Logger.getLogger(getClass()).info("Event count: " + eventCount + " events.");
		if (eventCount > 0) {
			Logger.getLogger(getClass()).info("Time per event: " + formatter.format((double) totalOverheadInNano / (eventCount * 1000000)) + " milis.");
		}

	}

	ObjectOutputStream outStream = null;

	private void internalSent(Object obj) {
		checkConnection();
		try {
			if (outStream == null) {
				outStream = new ObjectOutputStream(clientSocket.getOutputStream());
			}
			outStream.writeObject(obj);
		} catch (IOException e) {
			throw new NetworkException(e);
		}
	}

	private EventMessage getMessage() throws NetworkException {
		checkConnection();
		if (in == null) {
			try {
				in = new ObjectInputStream(clientSocket.getInputStream());
			} catch (Exception e) {
				throw new NetworkException(e);
			}
		}
		try {
			return (EventMessage) in.readObject();
		} catch (Exception e) {
			//
			// No connection - AUT stopped or cannot communicate.
			//
			if (e instanceof SocketException) {
				disconnect();
				return new ExitEventMessage();
			}
			throw new NetworkException(e);
		}
	}

	//
	// Code only used for generating event statistics
	//
	private static File csvFile = new File("d:/tests-instrument/eventtrace/events.csv");

	private static FileWriter csvWriter;

	static {
		try {
			csvWriter = new FileWriter(csvFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			//
			// 1. Open socket connection
			//
			socket = new ServerSocket(config.getPort());
			clientSocket = socket.accept();
			Logger.getLogger(WidgetInstrumentationConnector.class).info("Connection established.");
			isConnected = true;

			//
			// 2. Send agent configuration
			//
			internalSent(config);
			Logger.getLogger(WidgetInstrumentationConnector.class).debug(config.toString());
		} catch (IOException e) {
			throw new NetworkException(e);
		}

		while (this.isConnected == true) {
			//
			// Get message, record to output XML and transmit to registered listeners
			//
			EventMessage message = getMessage();

			//
			// Benchmarking
			//
			totalOverheadInNano += message.prevEventOverheadNano;
			eventCount++;

			try {
				csvWriter.write(formatter.format(((double) message.prevEventOverheadNano / 1000000)) + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}

			recordEvent(message);
			fireEventMessageReceived(message);

			//
			// The BufferedImage is null'ed so it does not consume memory
			//
			// if (message.screenShot != null) {
			// message.screenShot.flush();
			// message.screenShot = null;
			// }
		}

		try {
			clientSocket.close();
			socket.close();
			csvWriter.close();
		} catch (IOException e) {
			throw new NetworkException(e);
		}
	}

	public void addEventMessageListener(EventMessageListener l) {
		if (isConnected == true) {
			throw new NetworkException("Listeners can be added/removed only when thread is stopped");
		}
		listenerList.add(EventMessageListener.class, l);
	}

	public void removeEventMessageListener(EventMessageListener l) {
		if (isConnected == true) {
			throw new NetworkException("Listeners can be added/removed only when thread is stopped");
		}
		listenerList.remove(EventMessageListener.class, l);
	}

	protected void fireEventMessageReceived(EventMessage message) {
		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();
		// Process the listeners last to first, notifying
		// those that are interested in this event
		for (int i = listeners.length - 2; i >= 0; i -= 2) {
			if (listeners[i] == EventMessageListener.class) {
				event = new EventMessageReceivedEvent(message, this);
				((EventMessageListener) listeners[i + 1]).messageReceived(event);
			}
		}

//		if (event != null && event.getMessage() != null && event.getMessage().screenShot != null) {
//			event.getMessage().screenShot.flush();
//			event.getMessage().screenShot = null;
//		}
	}

	private void recordEvent(EventMessage message) {
		if (outputWriter == null) {
			return;
		}

		try {
			//
			// Write screenshot
			//
			if (message.screenShot != null) {
				// ImageIO.write(message.screenShot, "png", new File(screenshotDir + "/scr_" + message.id + ".png"));
			}

			eventsWrapper.events.add(message);

			if (message instanceof ExitEventMessage) {
				//
				// Now we marshal the event list
				//
				Logger.getLogger(WidgetInstrumentationConnector.class).debug("Marshalling event list...");
				marshaller.marshal(eventsWrapper, outputWriter);
				outputWriter.close();
			}
		} catch (Exception e) {
			throw new InstrumentationException(e);
		}
	}
}
