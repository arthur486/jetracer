/*
 * <Application Research Tools for GUI Driven Application Visualization and Testing>
 * Copyright (C) 2014,  Arthur Molnar (arthur@cs.ubbcluj.ro)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * If you want access to this program using a different license please contact the copyright holder
 *
 * Contributors:
 *    Arthur Molnar 
 */
package edu.art.module.jetracer.host.server.event;

import java.util.EventObject;

import edu.art.module.jetracer.common.EventMessage;

public class EventMessageReceivedEvent extends EventObject {

	private static final long serialVersionUID = -4932182688200708739L;

	private EventMessage message;

	public EventMessageReceivedEvent(Object source) {
		super(source);
	}

	public EventMessageReceivedEvent(EventMessage message, Object source) {
		super(source);
		this.message = message;
	}

	public EventMessage getMessage() {
		return message;
	}
}
